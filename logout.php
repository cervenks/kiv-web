<?php
include_once 'controllers/LoginController.php';
include_once 'models/BaseModel.php';
include_once 'models/UserModel.php';
include_once 'models/settings.inc.php';

$lc = new LoginController();
$lc->logout();
header("Location: index.php");