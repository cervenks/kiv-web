<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 01.12.2018
 * Time: 20:36
 */

class menuView
{
    private $pdo;


    //konstruktor
    function __construct()
    {
        include_once "models/BaseModel.php";
        include_once "models/settings.inc.php";
        include_once "models/UserModel.php";

        $this->pdo = new userModel();
        $this->pdo->Connect();
    }


    //zavola model pro zjisteni role
    private function get_role() {
        

        if(isset($_COOKIE["login"])) {
            return $this->pdo->get_role($_COOKIE["login"]);
        } else {
            return 0;
        }
    }

    //priprava parametru pro twig
    //odkazy v menu vlevo
    public function get_menu_items_left() {
        $role = $this->get_role();

        $items = array();
        $items['index'] = array('link' => 'index.php','title' => 'Zobrazit příspěvky');

        if($role >= 1) {
            $items['new_post'] = array('link' => 'edit_post.php','title' => 'Nový příspěvek');
            $items['my_posts'] = array('link' => 'my_posts.php','title' => 'Moje příspěvky');
        }

        if($role >= 3) {
            $items['user_administration'] = array('link' => 'user_administration.php','title' => 'Správa uživatelů');
        }

        return $items;

    }


    //priprava parametru pro twig
    //odkazy v menu vpravo
    public function get_menu_items_right() {
        $role = $this->get_role();
        $items = array();

        if($role == 0) {
            $items['login'] = array('link' => 'login.php','title' => 'Přihlásit se');
            $items['singup'] = array('link' => 'signup.php','title' => 'Registrovat se');
        } else {
            $items['login'] = array('link' => 'logout.php','title' => 'Odhlásit se');
        }

        return $items;
    }


    //priprava parametru pro twig
    //info o prihlasenem uzivateli
    public function get_login_info() {
        $role = $this->get_role();
        
        if($role == 0) {
            return "Nepřihlášen";
        }

        $role_string = $this->pdo->get_role_from_int($role);
        $login = $_COOKIE["login"];

        return "Přihlášen jako '$login'; Role: '$role_string'.";
        
    }


    //vypise menu
    public function echo_menu($twig) {
        echo $twig->render('menu.html.twig',['menu_items_left' => $this->get_menu_items_left(), 'menu_items_right' => $this->get_menu_items_right(),
            'log_text' => $this->get_login_info()]);
    }





}