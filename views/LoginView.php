<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 07.12.2018
 * Time: 21:55
 */

class LoginView
{
    //vypise formular pro prihlasovani
    public function echo_login_form($twig) {
        echo $twig->render('login.html.twig', ['login_handler' => '','login_name' => 'login', 'password_name' => 'password']);
    }

    //vypise formular pro registraci
    public function echo_signup_form($twig) {
        echo $twig->render('signup.html.twig', ['login_handler' => '','login_name' => 'login',
            'password_name' => 'password', 'password_confirm_name' => 'password_confirm']);
    }
}