<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 02.12.2018
 * Time: 11:34
 */

class userAdministrationView {

    private $pdo;

    //konstruktor
    function __construct()
    {
        include_once "models/BaseModel.php";
        include_once "models/settings.inc.php";
        include_once "models/UserModel.php";

        $this->pdo = new userModel();
        $this->pdo->Connect();
    }



    //nacte uzivatele z databaze pro ucely vytvoreni tabulky administrace
    private function get_users_from_db() {
        $users = $this->pdo->get_all_users();
        return $users;
    }


    //vrati nazev role podle id
    private function get_role($int_role) {
        $role = $this->pdo->get_role_from_int($int_role);
        return $role;
    }


    //priprava parametru pro twig
    //sestavi tabulku pro administraci uzivatelu
    public function get_all_users() {
        $i = 1;
        $rvalue = array();
        $users = $this->get_users_from_db();

        foreach($users as $user) {

            $login = $user["Login"];
            $role = $this->get_role($user["Role ID"]);
            $blocked = $user["Blocked"];


            $rvalue[$i] = array("i" => $i,"login" => $login,"role" => $role,"blocked" => $blocked);
            $i++;
        }

        return $rvalue;
    }



    //vypise tabulku pro administraci uzivatelu
    public function echo_ua($twig) {
            echo $twig->render('user_administration.html.twig', ['users' => $this->get_all_users()]);
    }


}