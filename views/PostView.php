<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 03.12.2018
 * Time: 17:58
 */

class PostView
{

    private $pm;
    private $um;
    private $rm;
    private $rv;


    //konstruktor
    public function __construct()
    {
        include_once "models/BaseModel.php";
        include_once "models/settings.inc.php";
        include_once "models/PostModel.php";
        include_once "models/UserModel.php";
        include_once "models/RatingModel.php";
        include_once "views/RatingView.php";


        $this->pm = new PostModel();
        $this->um = new UserModel();
        $this->rm = new RatingModel();
        $this->um->Connect();
        $this->pm->Connect();
        $this->rm->Connect();

        $this->rv = new RatingView();
    }


    //zavola model pro ziskani uzivatele z id
    private function get_username_from_id($id)
    {
        return $this->um->load_user_from_id($id)["Login"];
    }


    //zavola model pro ziskani nazvu stavu podle id
    private function get_state_from_id($id)  {
        return $this->pm->load_state_from_id($id)["Title"];
    }


    //priprava parametru pro twig
    //dosazeni do tabulky pro vypsani prispevku
    //vrati prispevky na zaklade stavu
    public function get_index_table_items($stateID)
    {
        $posts = $this->pm->get_posts_by_state($stateID);
        $rvalue = array();
        $i = 1;
        $state = $this->get_state_from_id($stateID);

        foreach ($posts as $post) {
            $login = $this->get_username_from_id($post["Author ID"]);
            $rvalue[$i] = array("i" => $i, "id" => $post["ID"], "author" => $login, "title" => $post["Title"], 'state' => $state, 'avg' => $this->rv->calculate_total_avg($post["ID"]));
            $i++;
        }

        return $rvalue;
    }


    //priprava parametru pro twig
    //zajisti spravne vypsani informaci u prispevku
    public function get_post_info($id)
    {
        $post = $this->pm->get_post($id);
        $rvalue = array();

        $rvalue['title'] = $post['Title'];
        $rvalue['abstract'] = $post['Abstract'];
        $rvalue['state'] = $post['State ID'];

        return $rvalue;
    }

    //priprava parametru pro twig
    //vrati prispevky uzivatele
    public function get_authors_posts($id) {

        $posts = $this->pm->get_authors_posts($id);

        $rvalue = array();
        $i = 1;

        foreach ($posts as $post) {
            $state = $this->get_state_from_id($post["State ID"]);
            $rvalue[$i] = array("i" => $i, "id" => $post["ID"], "state" => $state, "title" => $post["Title"]);
            $i++;
        }

        return $rvalue;

    }


    //vrati stav prispevku
    public function get_state($id) {
        return $this->pm->get_post($id)["State ID"];
    }

    //vrati prispevky uzivatele na zaklade loginu
    public function get_authors_posts_by_login($login) {
        $id = $this->um->load_user($login)["ID"];
        return $this->get_authors_posts($id);
    }


    //vrati cestu k souboru pro prispevek
    public function get_file_path($post_id) {
        $file_name = $this->pm->get_post($post_id)["File Path"];
        return "files/$post_id/$file_name";

    }


    //vrati vsechny prispevky, ktere recenzent jeste nezhodnotil (a byly mu prideleny)
    public function get_posts_to_review($reviewerID) {
    $assignments = $this->rm->load_assignments_by_reviewer($reviewerID);

    $rvalue = array();
    $i = 1;

    foreach ($assignments as $assignment) {
        $post = $this->pm->get_post($assignment["Post ID"]);
        if($post["State ID"] != 2) {
            continue;
        }
        $login = $this->get_username_from_id($post["Author ID"]);
        $state = $this->get_state_from_id($post["State ID"]);
        $rvalue[$i] = array("i" => $i, "id" => $post["ID"], "author" => $login, "title" => $post["Title"], 'state' => $state, 'avg' => $this->rv->calculate_total_avg($post["ID"]));
        $i++;
    }

        return $rvalue;
    }


    //vrati vsechny prispevky, ktere recenzent jeste nezhodnotil (a byly mu prideleny) na zaklade loginu
    public function get_posts_to_review_by_login($reviewer) {
        $id = $this->um->load_user($reviewer)["ID"];
        return $this->get_posts_to_review($id);
    }


    //vypise uvodni stranu aplikace
    public function echo_index($twig) {
        include_once "controllers/LoginController.php";
        
        //Zjisteni opravneni uzivatele
        $lc = new LoginController();
        $role = $lc->get_role();
        
        
        //zobrazeni prislusnych tabulek
        if($role >= 2) {
            echo $twig->render('post_table.html.twig',['head' => 'Přidělené příspěvky k hodnocení','posts' => $this->get_posts_to_review_by_login($_COOKIE["login"])]);
        }

        if($role >= 3) {
            echo $twig->render('post_table.html.twig',['head' => 'Příspěvky, ke kterým je nutné přiřadit recenzenty','posts' => $this->get_index_table_items(1)]);
        }

        echo $twig->render('post_table.html.twig',['head' => 'Veřejné příspěvky','posts' => $this->get_index_table_items(3)]);

        if($role >= 3) {
            echo $twig->render('post_table.html.twig',['head' => 'Právě hodnocené příspěvky','posts' => $this->get_index_table_items(2)]);
        }

        if($role >= 3) {
            echo $twig->render('post_table.html.twig',['head' => 'Odmítnuté příspěvky','posts' => $this->get_index_table_items(4)]);
        }

    }


    //vypise formular pro editace/vytvoreni prispevku
    public function echo_edit_post($twig) {
        //Rozdelime celou funkci na dve casti, protoze muzeme bud prispevek bud vytvaret nebo editovat

        if(isset($_GET["id"]))  //To zjistime pomoci toho ifu
        {
            //Jedna se o editaci, je nastavene id
            $post = $this->get_post_info($_GET["id"]); //nacteme prispevek z databaze

            if($post["state"] == 1) {
                echo $twig->render("edit_post.html.twig",['head' => 'Editovat příspěvek','title' => $post['title'], 'abstract' => $post['abstract']]);
            }  else {
                header("Location: view_post.php?id=".$_GET["id"]);
            }

        }
        else
        {
            //Jedna se o novy prispevek, nechame vse prazdne
            echo $twig->render("edit_post.html.twig",['head' => 'Vytvořit nový příspěvek']);
        }
    }


    //vypise vsechny prispevky uzivatele
    public function echo_my_posts($twig) {

        echo $twig->render('my_posts.html.twig',['posts' => $this->get_authors_posts_by_login($_COOKIE["login"])]);
    }


    //vypise prispevek pro ostatni uzivatele
    public function echo_view_post($twig) {
        $post = $this->get_post_info($_GET["id"]);
        $file = $this->get_file_path($_GET["id"]);

        echo $twig->render("view_post.html.twig", ['head' => 'Náhled příspěvku', 'title' => $post['title'], 'abstract' => $post['abstract'], 'file_path' => $file]);
    }



}