<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 05.12.2018
 * Time: 18:25
 */

class RatingView
{

    private $pm;
    private $um;
    private $rm;


    //konstruktor
    function __construct()
    {
        include_once "models/BaseModel.php";
        include_once "models/settings.inc.php";
        include_once "models/PostModel.php";
        include_once "models/UserModel.php";
        include_once "models/RatingModel.php";

        $this->pm = new PostModel();
        $this->um = new UserModel();
        $this->rm = new RatingModel();
        $this->um->Connect();
        $this->pm->Connect();
        $this->rm->Connect();

    }


    //tuto funkce aplikace pouziva, kdyz chce sestavit tabulku pro prideleni recenzenta k prispevku
    //nejdrive pro select vybrani novehp recemzemta nacte vsechny recenzenty a pak ty, kteri jiz jsou prideleni z tohoto seznamu odebira
    //Odebrani zajisti tato metoda
    private function remove_from_reviewers($reviewers, $assignment)
    {
        foreach ($reviewers as $i => $reviewer) {
            if ($reviewer["ID"] == $assignment["Reviewer ID"]) {
                unset($reviewers[$i]);
                return $reviewers;
            }
        }
    }


    //funkce vyuzivana pri serazeni hodnoceni
    //aplikace nacte z databaze vsechny typy hodnoceni, ale musi je seradit vzdy stejne, aby hodnoceni bylo ve spravnem poradi
    private static function compare_ratings($a, $b)
    {
        if ($a["Rating Type ID"] >= $b["Rating Type ID"]) {
            return 1;
        } else {
            return -1;
        }
    }

    //spocita prumerne hodnoceni clanku
    public function calculate_total_avg($postID) {
        $assignments = $this->rm->load_assignments_by_post($postID);
        $count = 0;
        $sum = 0;
        foreach($assignments as $assignment) {
            if($assignment == null) {
                continue;
            }

            $ratings = $this->rm->load_ratings_by_assignment($assignment["ID"]);

            foreach($ratings as $rating) {
                if($rating == null) {
                    continue;
                }

                $sum += $rating["Value"];
                $count++;
            }
        }

        if($count == 0) {
            return "N/A";
        }
        return number_format((float)($sum / $count), 2, '.', '');
    }


    //spocita prumerne hodnoceni jednoho typu u jednoho clanku
    public function calculate_avg_by_rating_type($postID, $rating_type_ID) {
        $assignments = $this->rm->load_assignments_by_post($postID);
        $count = 0;
        $sum = 0;

        foreach($assignments as $assignment) {
            if($assignment == null) {
                continue;
            }

            $rating = $this->rm->load_ratings_by_assignment_and_type($assignment["ID"],$rating_type_ID);
            if($rating == false) {
                continue;
            }

            $sum += $rating["Value"];
            $count++;
        }
        if($count == 0) {
            return "N/A";
        }
        return number_format((float)($sum / $count), 2, '.', '');

    }

    //priprava parametru pro twig
    //radek pro prumerne hodnoceni
    public function get_avg_row($postID) {
        $titles = $this->get_full_rating_titles();
        $rvalue = array();

        foreach($titles as $title) {
            if($title == null) {
                continue;
            }

            $rvalue[$title["ID"]] = $this->calculate_avg_by_rating_type($postID,$title["ID"]);

        }

        $rvalue["avg"] = $this->calculate_total_avg($postID);

        return $rvalue;
    }


        //priprava parametru pro twig
    //sestavi select vybrani noveho recenzenta
    //zaroven sestavi informace pro tabulku prideleni recenzenta k prispevku
    public function get_reviewers($postID)
    {

        $reviewers = $this->um->load_users_with_permission(2);
        $assignments = $this->rm->load_assignments_by_post($postID);
        $i = 1;
        $rows = array();

        foreach ($assignments as $assignment) {
            $reviewers = $this->remove_from_reviewers($reviewers, $assignment);
            $login = $this->um->load_user_from_id($assignment["Reviewer ID"])["Login"];
            $ratings_sql = $this->rm->load_ratings_by_assignment($assignment["ID"]);
            if ($ratings_sql == null) {
                $ratings = -1;
            } else {
                usort($ratings_sql, array("RatingView", "compare_ratings"));
                $ratings = array();
                foreach ($ratings_sql as $rating_sql) {
                    $ratings[$rating_sql["Rating Type ID"]] = $rating_sql["Value"];
                }
                $average = array_sum($ratings) / count($ratings);
                $ratings["avg"] = $average;
            }

            $rows[$i] = array("i" => $i, "reviewer" => $login, "ratings" => $ratings, "id" => $assignment["ID"]);
            $i++;
        }


        return array("Available reviewers" => $reviewers, "Rows" => $rows);


    }

    //vrati vsechny nazvy hodnoceni
    public function get_rating_titles()
    {
        $rvalue = array();
        $ratings_sql = $this->rm->load_rating_titles();
        foreach ($ratings_sql as $rating_sql) {
            array_push($rvalue, $rating_sql["Title"]);
        }
        return $rvalue;
    }


    //vrati pocet parametru hodnoceni
    public function get_rating_count()
    {
        return count($this->get_rating_titles());
    }

    //vrati prirazeni na zaklade recenzenta a prispevku
    public function get_assignment($postID, $login)
    {
        $userID = $this->um->load_user($login)["ID"];
        $assignment = $this->rm->get_assignment($userID, $postID);
        return $assignment;
    }

    //priprava parametru pro twig
    //tabulka pro hodnoceni
    public function get_full_ratings($postID, $login)
    {
        $post = $this->pm->get_post($postID);
        if ($post["State ID"] != 2) {
            return -1;
        }


        $assignment = $this->get_assignment($postID, $login);

        if ($assignment == null) {
            return -1;
        }

        if ($assignment["Rating Done"] == 1) {
            return -1;
        }

        return $this->rm->load_rating_titles();
    }


    //zkontroluje, jestli recenzent jiz prispevek nezhodnotil
    public function check_rating_done($postID)
    {
        $posts = $this->rm->load_assignments_by_post($postID);
        $rvalue = 1;

        foreach ($posts as $post) {
            if ($post["Rating Done"] != 1) {
                $rvalue = 0;
            }
        }

        return $rvalue;
    }



    //vrati nazvy hodnoceni
    public function get_full_rating_titles()
    {
        return $this->rm->load_rating_titles();
    }


    //vypise tabulku pro prirazeni recenzenta k prispevku
    public function echo_reviewer_table($twig, $role, $post_view)
    {
        $rating_titles = $this->get_rating_titles(); //Nacteni nazvu hodnoceni (sloupce tabulky)
        //Nacteni vsech moznych recenzentu... recenzenti jsou rozdeleni na dve podskupiny
        //1. skupina "Available reviewers" jsou recenzenti zatim neprirazeni k tomutu prispevku
        //Tato skupina se objevi jako moznost v selectu
        //2. skupina "Rows" -> prideleni recenzenti, radky v tabulce
        $rvalue = $this->get_reviewers($_GET["id"]);

        $assignments = $rvalue["Rows"];
        $reviewers = $rvalue["Available reviewers"];
        $width = count($rating_titles) + 3; //sirka tabulka, pocet hodnocei + tlacitka + jmeno hodnotitele
        $avg = $this->get_avg_row($_GET["id"]);

        if ($role >= 3) { //zda-li je uzivatel admin a muze s tabulkou manipulovat
            $admin_row = 1;
        } else {
            $admin_row = 0;
        }

        if ($post_view->get_state($_GET["id"]) == 2 && $this->check_rating_done($_GET["id"]) == 1) { //Kontrola, jestli muzeme povolit klik na prijmout a odmitnout
            $disabled1 = 0;
        } else {
            $disabled1 = 1;
        }


        if (count($assignments) < 3 || $post_view->get_state($_GET["id"]) > 1) { //kontrola, jestli muzeme povolit klik na tlacitko "povolit hodnoceni"
            $disabled2 = 1;
        } else {
            $disabled2 = 0;
        }

        echo $twig->render("reviewer_table.html.twig", ["ratings" => $rating_titles, "assignments" => $assignments,
            "available_reviewers" => $reviewers, "admin_row" => $admin_row, "width" => $width, "postID" => $_GET["id"],
            "disabled1" => $disabled1, "disabled2" => $disabled2, "avgs" => $avg]);

    }


    //vypise tabulku pro hodnoceni
    public function echo_rating_table($twig, $role) {
        if ($role >= 2) { //hodnotit mohou pouze recenzenti
            $ratings = $this->get_full_ratings($_GET["id"], $_COOKIE["login"]); //Seznam vsech druhu hodnoceni i s ID s tabulce
            if ($ratings != -1) { //V pripade ze recenzent uz hodnotil, nebo mu neni prideleno hodnoceni toho prispevku, vrati metoda vyse -1
                echo($twig->render("rating_table.html.twig", ["postID" => $_GET["id"],"ratings" => $ratings]));
            }
        }
    }


}