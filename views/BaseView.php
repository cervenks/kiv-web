<?php

class BaseView
{
    //vypise hlavicku stranky
    public function echo_head($twig, $title, $import) {

        echo $twig->render("head.html.twig", ["Title" => "$title", "Import" => $import]);
    }

    //vypise konec stranky
    public function echo_bottom($twig) {
        echo $twig->render("bottom.html.twig");
    }

}