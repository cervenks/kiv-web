<?php

require_once __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

include_once 'views/BaseView.php';
include_once 'views/MenuView.php';
include_once 'views/PostView.php';

$bv = new BaseView();
$mv = new MenuView();
$pv = new PostView();

$bv->echo_head($twig,"Příspěvky","");
$mv->echo_menu($twig);
echo("<br><br><br>");
$pv->echo_index($twig);
$bv->echo_bottom($twig);





