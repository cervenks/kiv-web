<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 01.12.2018
 * Time: 17:42
 */

class userModel extends baseModel
{
    //nacte uzivatele na zaklade loginu
    public function load_user($login) {
        $table_name = "user";
        $columns = "*";
        $where = array(array("column" => "Login","value" => $login, "symbol" => "="));
        $rvalue = $this->DBSelectOne($table_name,$columns,$where);

        return $rvalue;
    }


    //vytvori v databazi noveho uzivatele
    public function create_user($login, $hashed_password, $role) {
        $table_name = "user";
        $item = array();
        $item["Login"] = $login;
        $item["Password"] = $hashed_password;
        $item["Role ID"] = $role;

        $this->DBInsert($table_name,$item);
    }


    //zjisti roli daneho uzivatele
    public function get_role($login) {
        $table_name = "user";
        $columns = "*";
        $where = array(array("column" => "Login","value" => $login, "symbol" => "="));
        $rvalue = $this->DBSelectOne($table_name,$columns,$where);


        return $rvalue["Role ID"];
    }


    //vrati vsechny uzivatele
    public function get_all_users() {
        $table_name = "user";
        $columns = "*";
        $where = null;
        $rvalue = $this->DBSelectAll($table_name,$columns,$where);
        return $rvalue;
    }


    //zjisti nazev role pomoci id
    public function get_role_from_int($role) {
        $table_name = "role";
        $columns = "*";
        $where = array(array("column" => "ID", "value" => $role, "symbol" => "="));
        $rvalue = $this->DBSelectOne($table_name,$columns,$where);
        return $rvalue["Title"];

    }

    //nastavi roli uzivatele
    public function set_role($role, $user) {
        $table_name = "user";
        $column = "Role ID";
        $id = $this->load_user($user)["ID"];

        $this->DBUpdate($table_name,$id,$column,$role);
    }


    //zablokuje nebo odblokuje uzivatele
    public function set_block($block, $user) {
        $table_name = "user";
        $column = "Blocked";
        $id = $this->load_user($user)["ID"];

        $this->DBUpdate($table_name,$id,$column,$block);
    }


    //nacte uzivatele pomoci id
    public function load_user_from_id($id) {
        $table_name = "user";
        $columns = "*";
        $where = array(array("column" => "id","value" => $id, "symbol" => "="));
        $rvalue = $this->DBSelectOne($table_name,$columns,$where);

        return $rvalue;
    }


    //nacte vsechny uzivatele, ktere maji alespon zadanou roli
    public function load_users_with_permission($roleID) {
        $table_name = "user";
        $columns = "*";
        $where = array(array("column" => "Role ID","value" => $roleID, "symbol" => ">="));
        $rvalue = $this->DBSelectAll($table_name,$columns,$where);

        return $rvalue;
    }


    //smaze uzivatele
    public function delete_user($user) {
        $table_name = "user";
        $where = array(array("column" => "Login", "value" => $user, "symbol" => "="));

        $this->DBDelete($table_name,$where,null);
    }
}