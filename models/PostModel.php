<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 03.12.2018
 * Time: 17:48
 */

class PostModel extends BaseModel
{



    //vrati vsechny prispevky se zadanym stavem
    public function get_posts_by_state($state)
    {
        $table_name = "post";
        $columns = "*";
        $where = array(array("column" => "State ID", "value" => $state, "symbol" => "="));

        return $this->DBSelectAll($table_name, $columns, $where);
    }


    //vrati vsechny prispevky
    public function get_all_posts() {
        $table_name = "post";
        $columns = "*";
        $where = null;

        return $this->DBSelectAll($table_name, $columns, $where);
    }

    //prida do databaze novy prispevek
    public function create_new_post($title, $author, $abstract) {
        $table_name = "post";
        $item = array();
        $item['Title'] = $title;
        $item['Author ID'] = $author;
        $item['Abstract'] = $abstract;
        $item['State ID'] = 1;

        return $this->DBInsert($table_name,$item);
    }


    //prida do databaze nazev vytvoreneho souboru (puvodne to mela byt cela cesta, ale to se ukazalo jako neprakticke)
    public function set_file_path($id,$path) {
        $table_name = "Post";
        $column = "File Path";

        $this->DBUpdate($table_name,$id,$column,$path);
    }


    //vrati prispevek na zaklade id
    public function get_post($id) {
        $table_name = "Post";
        $columns = "*";
        $where = array(array("column" => "ID","value" => $id, "symbol" => "="));
        $rvalue = $this->DBSelectOne($table_name,$columns,$where);

        return $rvalue;
    }


    //upravi prispevek, ale nezmeni soubor (ten se pripadne zmeni pomoci set_file_path)
    public function edit_post_without_file($id, $title, $abstract) {
        $table_name = "Post";
        $column = "Title";
        $value = $title;

        $this->DBUpdate($table_name,$id,$column,$value);

        $column = "Abstract";
        $value = $abstract;

        $this->DBUpdate($table_name,$id,$column,$value);
    }


    //vrati prispevky daneho autora
    public function get_authors_posts($authorID) {
        $table_name = "Post";
        $column = "*";
        $where = array(array("column" => "Author ID", "value" => $authorID, "symbol" => "="));

        return $this->DBSelectAll($table_name,$column,$where);
    }


    //vrati stav (jmeno stavu) na zaklade id
    public function load_state_from_id($id) {
        $table_name = "State";
        $columns = "*";
        $where = array(array("column" => "id","value" => $id, "symbol" => "="));
        $rvalue = $this->DBSelectOne($table_name,$columns,$where);

        return $rvalue;
    }

    //nastavi stav u daneho prispevku
    public function set_state($id, $stateID) {
        $table_name = "Post";
        $column = "State ID";
        $value = $stateID;

        $this->DBUpdate($table_name,$id,$column,$value);

    }




}