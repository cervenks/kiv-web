<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 05.12.2018
 * Time: 18:21
 */

class RatingModel extends BaseModel
{

    //nacte vsechny prirazeni jednoho recenzenta k prispevkum
    public function load_assignments_by_reviewer($reviewerID) {
        $table_name = "Assignment";
        $columns = "*";
        $where = array(array("column" => "Reviewer ID","value" => $reviewerID, "symbol" => "="));

        return $this->DBSelectAll($table_name,$columns,$where);
    }


    //nacte prirazeni vsech recenzentu k jednomu prispevku
    public function load_assignments_by_post($postID) {
        $table_name = "Assignment";
        $columns = "*";
        $where = array(array("column" => "Post ID","value" => $postID, "symbol" => "="));

        return $this->DBSelectAll($table_name,$columns,$where);
    }


    //nacte vsechna hodnoceni jednoho autora k jednomu prispevku (pomoci prirazeni tohoto autora k prispevku)
    public function load_ratings_by_assignment($assignmentID) {
        $table_name = "Rating";
        $columns = "*";
        $where = array(array("column" => "Assignment ID","value" => $assignmentID, "symbol" => "="));

        return $this->DBSelectAll($table_name,$columns,$where);
    }

    //nacte vsechna hodnoceni jednoho autora k jednomu prispevku (pomoci prirazeni tohoto autora k prispevku)
    public function load_ratings_by_assignment_and_type($assignmentID, $ratingTypeID) {
        $table_name = "Rating";
        $columns = "*";
        $where = array(array("column" => "Assignment ID","value" => $assignmentID, "symbol" => "="),
            array("column" => "Rating Type ID","value" => $ratingTypeID, "symbol" => "="));

        return $this->DBSelectOne($table_name,$columns,$where);
    }

    //nacte nazvy parametru hodnoceni
    public function load_rating_titles() {
        $table_name = "rating_type";
        $columns = "*";
        $where = null;

        return $this->DBSelectAll($table_name,$columns,$where);
    }


    //priradi recenzenta k prispevku
    public function create_assignment($reviewerID, $postID) {
        $table_name = "assignment";
        $item = array();
        $item["Reviewer ID"] = $reviewerID;
        $item["Post ID"] = $postID;
        $item["Rating Done"] = 0;

        return $this->DBInsert($table_name,$item);
    }

    //odebere prirazeni recenzenta k prispevku
    public function remove_assignment($assignmentID) {
        $table_name = "assignment";
        $where = array(array("column" => "id", "value" => $assignmentID, "symbol" => "="));

        $this->DBDelete($table_name,$where,null);
    }


    //nacte prirazeni recenzenta k prispevku
    public function get_assignment($reviewerID, $postID) {
        $table_name = "Assignment";
        $columns = "*";
        $where = array(array("column" => "Reviewer ID","value" => $reviewerID, "symbol" => "="),
            array("column" => "Post ID", "value" => $postID, "symbol" => "="));

        return $this->DBSelectOne($table_name,$columns,$where);
    }


    //vytvori jedno ohodnoceni
    public function create_new_rating($assignmentID, $rating_type_ID, $value) {
        $table_name = "Rating";
        $item = array("Assignment ID" => $assignmentID, "Rating Type ID" => $rating_type_ID, "Value" => $value);

        return $this->DBInsert($table_name,$item);
    }


    //odebere vsechna ohodnoceni u jednoho autora k jednomu prispevku
    public function remove_ratings($assignmentID) {
        $table_name = "rating";
        $where = array(array("column" => "Assignment ID", "value" => $assignmentID, "symbol" => "="));

        $this->DBDelete($table_name,$where,null);
    }


    //nastavi parametr dokonceni u prispevku
    public function set_done($id) {
        $table_name = "Assignment";
        $column = "Rating Done";
        $value = 1;

        $this->DBUpdate($table_name,$id,$column,$value);

    }



}