<?php

include_once 'controllers/LoginController.php';
include_once 'controllers/RatingController.php';
include_once 'views/RatingView.php';
include_once 'models/BaseModel.php';
include_once 'models/UserModel.php';
include_once 'models/PostModel.php';
include_once 'models/settings.inc.php';
include_once 'models/RatingModel.php';


$lc = new LoginController();
$role = $lc->get_role();
$rv = new RatingView();
$rc = new RatingController();
$postID = $_GET["id"];

if ($role >= 2) {
    $titles = $rv->get_full_rating_titles();
    $user_ratings = array();
    $assignmentID = $rv->get_assignment($postID,$_COOKIE["login"])["ID"];

    foreach($titles as $title) {
        $typeID = $title["ID"];
        $value = $_POST["r$typeID"];
        $rc->create_new_rating($assignmentID,$typeID,$value);
    }
    $rc->set_assignment_done($assignmentID);

}

header("Location: view_post.php?id=$postID");
exit;
