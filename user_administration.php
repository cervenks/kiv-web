<?php
require_once __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);


include_once 'controllers/LoginController.php';
include_once 'views/MenuView.php';
include_once 'views/UserAdministrationView.php';
include_once 'views/BaseView.php';

$bv = new BaseView();
$lc = new LoginController();
$mv = new MenuView();
$uav = new UserAdministrationView();

//Uzivatel s roli mensi nez admin bude presmerovan
$lc->check_permission(3,"index.php");

//Metoda, ktera zkontroluje vsechny udalosti v user administration
$lc->user_admin_check();

$bv->echo_head($twig,"Administrace","");
$mv->echo_menu($twig);
$uav->echo_ua($twig);
$bv->echo_bottom($twig);






