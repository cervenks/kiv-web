<?php

require_once __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

include_once 'controllers/LoginController.php';
include_once 'views/MenuView.php';
include_once 'views/BaseView.php';
include_once 'views/LoginView.php';

$lc = new LoginController();
$mv = new MenuView();
$bv = new BaseView();
$lv = new LoginView();

//kontrola prihlaseni, prihlaseny uzivatel by se nemel znovu prihlasit predtim nez se odhlasi
$lc->check_if_not_logged("index.php");

//Kontrola, jestl se nejedna o pozadavek na prihlaseni, pokud probehne spravne prihlaseni, posleme uzivatele na index.php
$lc->check_logging("index.php");

$bv->echo_head($twig,"Přihlášení",1);
$mv->echo_menu($twig);
$lv->echo_login_form($twig);
$bv->echo_bottom($twig);



