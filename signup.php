<?php
require_once __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

include_once 'controllers/LoginController.php';
include_once 'views/BaseView.php';
include_once 'views/LoginView.php';
include_once 'views/MenuView.php';


$lc = new LoginController();
$mv = new MenuView();
$bv = new BaseView();
$lv = new LoginView();

//Prihlaseny uzivatel by nemel mit moznost se zaregistrovat
$lc->check_if_not_logged("index.php");

//Kontrola, jestli nejde o pozadavek na registraci
$lc->check_registrating("index.php");

$bv->echo_head($twig,"Registrace",1);
$mv->echo_menu($twig);
$lv->echo_signup_form($twig);
$bv->echo_bottom($twig);


