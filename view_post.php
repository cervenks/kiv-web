<?php
require_once __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

include_once 'controllers/LoginController.php';
include_once 'controllers/RatingController.php';
include_once 'controllers/PostController.php';
include_once 'views/MenuView.php';
include_once 'views/PostView.php';
include_once 'views/RatingView.php';
include_once 'views/BaseView.php';


$mv = new MenuView();
$pv = new PostView();
$rv = new RatingView();
$bv = new BaseView();
$lc = new LoginController();
$rc = new RatingController();
$pc = new PostController();
$role = $lc->get_role();

//Kontrola pozadavku na prideleni recenzenta k prispevku
$rc->check_new_assignment($role);

// -||- na odstraneni recenzenta
$rc->check_remove_assignment($role);

// Kontrola pozadavku na "upgrade" prispevku ze stavu 1 na 2 (t.j. umoznit recenzentum hodnocenim)
$pc->check_post_promotion($twig, $role);

$bv->echo_head($twig,"Náhled příspěvku",2);

$mv->echo_menu($twig);

//Vypsani zakladniho pole pro cteni prispevku
$pv->echo_view_post($twig);

//Vypsani tabulky s hodnocenim
$rv->echo_reviewer_table($twig,$role,$pv);

//Vypsani tabulky pro hodnoceni prispevku
$rv->echo_rating_table($twig, $role);

$bv->echo_bottom($twig);






