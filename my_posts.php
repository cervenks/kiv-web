<?php
require_once __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

include_once 'views/MenuView.php';
include_once 'views/PostView.php';
include_once 'views/BaseView.php';
include_once 'controllers/LoginController.php';

$bv = new BaseView();
$mv = new MenuView();
$pv = new PostView();
$lc = new LoginController();

//Kontrola prihlaseni, neprihlaseny uzivatel bude presmerovan na index.php
$lc->check_if_logged("index.php");

$bv->echo_head($twig,"Moje příspěvky","");
$mv->echo_menu($twig);
$pv->echo_my_posts($twig);
$bv->echo_bottom($twig);

