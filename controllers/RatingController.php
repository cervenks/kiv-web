<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 06.12.2018
 * Time: 11:11
 */

class RatingController
{

    private $rm;
    private $um;


    //konstruktor
    function __construct()
    {
        include_once "models/BaseModel.php";
        include_once "models/settings.inc.php";
        include_once "models/RatingModel.php";
        include_once "models/UserModel.php";

        $this->rm = new RatingModel();
        $this->rm->Connect();
        $this->um = new UserModel();
        $this->um->Connect();
    }


    //priradi recenzentu clanek k hodnoceni na zaklade id recenzenta
    public function create_assignment($reviewerID, $postID) {
        $this->rm->create_assignment($reviewerID,$postID);
    }

    //priradi recenzentu clanek k hodnoceni na zaklade loginu recenzenta
    public function create_assignment_by_login($reviewerLogin, $postID) {
        $reviewerID = $this->um->load_user($reviewerLogin)["ID"];
        $this->create_assignment($reviewerID,$postID);
    }


    //zrusi prirazeni recenzenta k prispevku, zaroven smaze vsechny jeho hodnoceni
    public function remove_assignment($assignmentID) {

        $this->rm->remove_assignment($assignmentID);
    }


    //zavola model kvuli vytvoreni noveho hodnoceni
    public function create_new_rating($assignmentID, $rating_type_ID, $value) {
        return $this->rm->create_new_rating($assignmentID,$rating_type_ID,$value);
    }


    //timto indikujeme, ze recenzent uz clanek ohodnotil
    public function set_assignment_done($assignmentID) {
        $this->rm->set_done($assignmentID);
    }


    //zkontroluje, jestli prave spustena stranka neni pozadavkem na vytvoreni noveho prirazeni
    public function check_new_assignment($role) {
        if($role < 3) { //Na vytvoreni assignemntu (prideleni recenzenta k prispevku) je treba role admina
            return;
        }

        if (isset($_POST["reviewer_login"]) && strcmp($_POST["reviewer_login"], "") != 0) { //Kontrola, jestli je validne vybran uzivatel
            $this->create_assignment_by_login($_POST["reviewer_login"], $_GET["id"]);
        }

    }

    //zkontroluje, jestli prave spustena stranka neni pozadavkem na odstraneni noveho prirazeni
    public function check_remove_assignment($role) {
        if($role < 3) { //Na odstraneni assignemntu (prideleni recenzenta k prispevku) je treba role admina
            return;
        }

        if (isset($_GET["e"]) && strcmp($_GET["e"], "ra") == 0) { //kontrala validniho eventu
            if(isset($_GET["as"])) { //kontrola validniho atributu
                $this->remove_assignment($_GET["as"]);
            }

        }
    }




}