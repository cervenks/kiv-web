<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 03.12.2018
 * Time: 17:44
 */

class PostController
{

    private $um;
    private $pm;

    //konstruktor
    public function __construct()
    {
        include_once "models/BaseModel.php";
        include_once "models/settings.inc.php";
        include_once "models/UserModel.php";
        include_once "models/PostModel.php";

        $this->um = new userModel();
        $this->um->Connect();
        $this->pm = new postModel();
        $this->pm->Connect();

    }


    //vytvori novy prispevek se zadanymi parametry
    public function create_new_post($title, $abstract, $file) {
        $author = $this->um->load_user($_COOKIE["login"])["ID"];

        $post = $this->pm->create_new_post($title,$author,$abstract);
        $file_path = $this->upload_pdf($post,$file);
        $this->pm->set_file_path($post,$file_path);


    }


    //upravi prispevek
    public function edit_post($id, $title, $abstract, $file) {
        $this->pm->edit_post_without_file($id,$title,$abstract);
        if($file["name"] != "") {
            $file_path = $this->upload_pdf($id,$file);
            $this->pm->set_file_path($id,$file_path);
        }

    }

    //nahraje soubor pro prispevek
    private function upload_pdf($post_id, $file) {
        $name = $file["name"];

        mkdir("files/$post_id");
        move_uploaded_file($file["tmp_name"],"files/$post_id/$name");

        return $name;
    }


    //zkontroluje, jestli autorem clanku je zadany uzivatel
    public function check_author($login, $post_id) {
        $user = $this->um->load_user($login);
        $post = $this->pm->get_post($post_id);

        if($post == null || $user == null) {
            return false;
        }

        if($user["ID"] == $post["Author ID"]) {
            return true;
        }
        return false;
    }


    //nastavi stav clanku
    public function set_state($postID, $stateID) {
        $this->pm->set_state($postID,$stateID);
    }


    //zkontroluje, jestli pristup na stranku edit_post je validni
    //editovat prispevek muze pouze autor
    public function check_if_author($site) {
        if(isset($_GET["id"]) == false) {
            return; //Pokud neni zadane id, jedna se o vytvoreni noveho prispevku a zadne presmerovani neni treba
        } else {
            if($this->check_author($_COOKIE["login"],$_GET["id"])) {
                return; //V poradku, uzivatel je autor
            }
        }

        header("Location: $site"); //Podminky nesplneny, uzivatel bude presmerovan
    }


    //zkontroluje, jestli prave spustena stranka neni pozadavkem na vytvoreni noveho prispevku
    public function check_if_new_post_was_created($old_edited) {
        //Nutnou podminkou je zde situace, ze se nejedna o pozadavek na editaci stareho
        if($old_edited == true) {
            return false;
        }

        //Dalsi podminkou je existence potrebnych $_POST
        if(isset($_POST["title"]) == false || isset($_POST["abstract"]) == false || isset($_FILES["file"]) == false) {
            return false;
        }

        //Nyni je vse splneno
        $this->create_new_post($_POST["title"],$_POST["abstract"],$_FILES["file"]);
        return true;
    }


    //zkontroluje, jestli prave spustena stranka neni pozadavkem na upravu stareho prispevku
    public function check_if_old_post_was_edited() {
        //1. podminka, musi byt deklarovany atribut id prispevku
        if(isset($_GET["id"]) == false) {
            return false;
        }


        //2. podminka je existence potrebnych $_POST
        if(isset($_POST["title"]) == false || isset($_POST["abstract"]) == false || isset($_FILES["file"]) == false) {
            return false;
        }

        //Pokud jsou obe podminky splneny, muzeme konstatovat, ze doslo k validnimu pozadavku na upravu prispevku
        //Prispevek tedy upravime
        $this->edit_post($_GET["id"],$_POST["title"],$_POST["abstract"],$_FILES["file"]);
        return true;

    }


    //zkontroluje, jestli prave spustena stranka neni pozadavkem na zmenu stavu clanku
    public function check_post_promotion($twig, $role) {
        if($role < 3) { //Role mensi nez admin nema opravneni
            return;
        }

        if(isset($_GET["e"])) { //pokud neni nastaveny event, nejedna se o pozadavek
            if(strcmp($_GET["e"], "pp") == 0) { //zmena stavu z 1 na 2
                $this->set_state($_GET["id"], 2);
            } else if(strcmp($_GET["e"], "ap") == 0) { //2 -> 3
                $this->set_state($_GET["id"], 3);
            } else if(strcmp($_GET["e"], "rp") == 0) {
                $this->set_state($_GET["id"], 4); //2 -> 4
            }
        }
    }

}