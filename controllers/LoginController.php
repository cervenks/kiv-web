<?php
/**
 * Created by PhpStorm.
 * User: stepa
 * Date: 01.12.2018
 * Time: 17:39
 */

class LoginController
{
    private $pdo;


    //Konstruktor
    public function __construct()
    {
        include_once "models/BaseModel.php";
        include_once "models/settings.inc.php";
        include_once "models/UserModel.php";
        $this->pdo = new UserModel();
        $this->pdo->Connect();
    }




    //overi platny login a heslo
    public function validate($login, $password) {
        $user = $this->pdo->load_user($login);
        //uzivatel nenalezen
        if($user == null) {
            return false;
        }

        //uzivatel blokovan
        if($user["Blocked"] == 1) {
            return false;
        }

        if(password_verify($password,$user["Password"])) {
            return true;
        }

        return false;


    }


    //vytvori uzivatele s defaultni roli autora
    public function create($login, $password) {
        $this->create_with_role($login, $password, 1);
    }

    //vytvori uzivatele s danou roli
    public function create_with_role($login, $password, $role) {
        $hashed_password = password_hash($password,PASSWORD_DEFAULT);
        $this->pdo->create_user($login,$hashed_password,$role);
    }


    //prihlasi uzivatele (prideli mu cookie)
    public function login($login) {
        $cookie_name = "login";
        $cookie_value = $login;
        setcookie($cookie_name,$cookie_value,time() + 86400, "/");
    }

    //odhlasi uzivatele
    public function logout() {
        setcookie("login","",time() - 3600);
    }


    //zkontroluje, jestli ma uzivatel danou roli (nebo vetsi)
    //kdyz ne, presmeruje ho na zadanou stranku
    public function check_permission($role, $site) {
        if ($role < $this->get_role()) {
            header("Location: $site");
            exit;
        }
    }


    //zavola model pro ziskani role uzivatele
    public function get_role() {
        if(!isset($_COOKIE["login"])) {
            return 0;
        }

        return $this->pdo->load_user($_COOKIE["login"])["Role ID"];
    }


    //nastavi roli
    public function set_role($role, $user) {
        $this->pdo->set_role($role,$user);
    }


    //zablokuje / odblokuje uzivatele
    public function toggle_block($user) {
        $blocked = $this->pdo->load_user($user)["Blocked"];

        if($blocked == 1) {
            $this->pdo->set_block(0,$user);
        } else {
            $this->pdo->set_block(1,$user);
        }
    }


    //smaze uzivatele
    public function delete_user($user) {
        $this->pdo->delete_user($user);
    }


    //zkontroluje, jestli je uzivatel prihlaseny
    //kdyz ne, presmeruje ho na zadanou stranku
    public function check_if_logged($site) {
        if(isset($_COOKIE["login"]) == false) {
            header("Location: $site");
            exit;
        }
    }


    //zkontroluje, jestli uzivatel neni prihlaseny (vhodne napriklad pro prihlasovani)
    public function check_if_not_logged($site) {
        if(isset($_COOKIE["login"]) == true) {
            header("Location: $site");
            exit;
        }
    }


    //Zkontroluje, jestli prave spustena stranka neni pozadavkem na prihlaseni
    public function check_logging($site) {
        if(isset($_POST['login']) && isset($_POST['password'])) { //Nastavene post
            if($this->validate($_POST['login'],$_POST['password'])) { //Kontrola prihlasovacich udaju
                $this->login($_POST['login']); //prihlaseni
                header("Location: $site");
                exit;
            }
        }
    }


    //Zkontroluje, jestli prave spustena stranka neni pozadavkem na registraci
    public function check_registrating($site) {
        if(isset($_POST['login']) && isset($_POST['password'])) {//Nastavene post
            $this->create($_POST['login'],$_POST['password']); //Zalozeni uctu
            $this->login($_POST['login']); //Prihlaseni
            header("Location: $site"); //Presmerovani
            exit;
        }
    }

    //Zkontroluje udalosti pro stranku user_administration
    public function user_admin_check() {
        if(isset($_GET["e"])) { //Je nastaveny event, beru to jako pozadavek k akci
            if(strcmp($_GET["e"],"role") == 0) { //event bude zmena role uzivatele
                if(isset($_GET["v"]) && isset($_GET["u"])) { //kontrola, ze je zvoleny uzivatel a role
                    $this->set_role($_GET["v"],$_GET["u"]); //nastaveni role
                    header("Location: user_administration.php"); //presmerovani
                    exit;
                }
            } else if(strcmp($_GET["e"],"block") == 0) { //chceme blokovat uzivatele
                if(isset($_GET["u"])) { //je nastaven uzivatel
                    $this->toggle_block($_GET["u"]); //blokujeme
                    header("Location: user_administration.php");
                    exit;
                }
            } else if(strcmp($_GET["e"],"delete") == 0) { //chceme uzivatele smazat
                if(isset($_GET["u"])) {
                    $this->delete_user($_GET["u"]);
                    header("Location: user_administration.php");
                    exit;
                }
            }
        }
    }




}