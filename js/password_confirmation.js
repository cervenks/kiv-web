var password = document.getElementById("inputPassword")
    , confirm_password = document.getElementById("inputConfirmPassword");

function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Hesla se neshodují");
    } else {
        confirm_password.setCustomValidity('');
    }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;