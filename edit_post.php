<?php
require_once __DIR__ . '/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

include_once 'controllers/LoginController.php';
include_once 'controllers/PostController.php';
include_once 'views/BaseView.php';
include_once 'views/MenuView.php';
include_once 'views/PostView.php';

$bv = new BaseView();
$mv = new menuView();
$pv = new PostView();
$lc = new LoginController();
$pc = new PostController();

//Kontrola prihlaseni, neprihlaseny uzivatel zde nema co delat, posleme ho na index.php
$lc->check_if_logged("index.php");

//Kontrola jestli je prihlaseny uzivatel autorem prispevku, funkce rozpozna i novy prispevek
$pc->check_if_author("index.php");

//Nejdrive zkontrolujeme, jestli otevreni stranky neni pozadavek na editovani prispevku
$old_edited = $pc->check_if_old_post_was_edited();

//Dale zkontrolujeme, zda-li se nejedna o pozadavek na vytvoreni prispevku noveho
$new_created = $pc->check_if_new_post_was_created($old_edited);


//Nyni sestavime stranku
$bv->echo_head($twig,"Vytvoření / Editace Příspěvku",2);
$mv->echo_menu($twig);
$pv->echo_edit_post($twig);
$bv->echo_bottom($twig);
